
package bufmgr;

import chainexception.ChainException;
import com.sun.net.httpserver.Filter;
import diskmgr.DiskMgr;
import diskmgr.FileIOException;
import diskmgr.InvalidPageNumberException;
import global.Minibase;
import global.Page;
import global.PageId;

import java.io.IOException;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Arrays;

public class BufMgr {
    private int numbufs = -666;
    private Page[] poolParty;
    private Descriptor[] bufDescr;
    private HashTable signInSheet;
    /**
     * Create the BufMgr object.
     * Allocate pages (frames) for the buffer pool in main memory and
     * make the buffer manage aware that the replacement policy is
     * specified by replacerArg (e.g., LH, Clock, LRU, MRU, LFU, etc.).
     *
     * @param numbufs number of buffers in the buffer pool
     * @param lookAheadSize: Please ignore this parameter
     * @param replacementPolicy Name of the replacement policy, that parameter will be set to "LFU" (you
    can safely ignore this parameter as you will implement only one policy)
     */
    public BufMgr(int numbufs, int lookAheadSize, String replacementPolicy) {
        if (!replacementPolicy.equals("LFU"))
            throw new InvalidParameterException("Replacement policy must be LFU");

        this.numbufs = numbufs;
        poolParty = new Page[numbufs];
        bufDescr = new Descriptor[numbufs];
        signInSheet = new HashTable();
    }
    /**
     * Pin a page.
     * First check if this page is already in the buffer pool.
     * If it is, increment the pin_count and return a pointer to this
     * page.
     * If the pin_count was 0 before the call, the page was a
     * replacement candidate, but is no longer a candidate.
     * If the page is not in the pool, choose a frame (from the
     * set of replacement candidates) to hold this page, read the
     * page (using the appropriate method from {\em diskmgr} package) and pin it.
     * Also, must write out the old page in chosen frame if it is dirty
     * before reading new page.__ (You can assume that emptyPage==false for
     * this assignment.)
     *
     * @param pageno page number in the Minibase.
     * @param page the pointer point to the page.
     * @param emptyPage true (empty page); false (non-empty page)
     */
    public void pinPage(PageId pageno, Page page, boolean emptyPage) throws ChainException {
        int index;
        try {
            index = signInSheet.retrieve(pageno.hashCode());
            bufDescr[index].pinCount++;
            bufDescr[index].frequency++;
            page.setPage(poolParty[index]);
        } catch (IllegalArgumentException e) {
            // Page not found
            for (int i = 0; i < getNumBuffers(); i++) {
                if (poolParty[i] == null) {
                    addToPool(pageno, page, i);
                    return;
                }
            }
            addToPool(pageno, page, evictLFU());
        }
    }


    // counts as a pin
    private void addToPool(PageId pageno, Page page, int frameNumber) {
        signInSheet.insert(pageno.hashCode(), frameNumber);
        poolParty[frameNumber] = new Page();
        try {
            Minibase.DiskManager.read_page(pageno, poolParty[frameNumber]);
        } catch (InvalidPageNumberException e) {
            e.printStackTrace();
        } catch (FileIOException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        bufDescr[frameNumber] = new Descriptor();
        bufDescr[frameNumber].pinCount = 1;
        bufDescr[frameNumber].pageId = new PageId();
        bufDescr[frameNumber].pageId.copyPageId(pageno);
        bufDescr[frameNumber].dirtyBit = false;
        bufDescr[frameNumber].frequency = 1;
        page.setPage(poolParty[frameNumber]);
    }

    /**
     * Removes LFU buffer from the pool
     * @return
     */
    private int evictLFU() throws ChainException {
        int index = -666;
        int value = Integer.MAX_VALUE;
        boolean worked = false;
        for (int i  = 0; i < bufDescr.length; i++) {
            if (value > bufDescr[i].frequency && bufDescr[i].pinCount == 0) {
                value = bufDescr[i].frequency;
                index = i;
                worked = true;
            }
        }

        if (!worked)
            throw new BufferPoolExceededException(null, "bufmgr.BufferPoolExceededException");

        evict(index);
        return index;
    }

    private void evict(int index) throws ChainException{
        Descriptor descriptor = bufDescr[index];

        if (descriptor.pinCount > 0)
            throw new PagePinnedException(null, "bufmgr.PagePinnedException");

        if (descriptor.dirtyBit) {
            flushPage(descriptor.pageId);
            descriptor.dirtyBit = false;
        }

		try {
			signInSheet.remove(descriptor.pageId.hashCode());
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}

        poolParty[index] = null;
        descriptor.index = -1;
        descriptor.pinCount = -1;
        descriptor.frequency = -1;
        descriptor.pageId = null;
        bufDescr[index] = null;

    }
    /**
     * Unpin a page specified by a pageId.
     * This method should be called with dirty==true if the client has
     * modified the page.
     * If so, this call should set the dirty bit
     * for this frame.
     * Further, if pin_count>0, this method should
     * decrement it.
     *If pin_count=0 before this call, throw an exception
     * to report error.
     *(For testing purposes, we ask you to throw
     * an exception named PageUnpinnedException in case of error.)
     *
     * @param pageno page number in the Minibase.
     * @param dirty the dirty bit of the frame
     */
    public void unpinPage(PageId pageno, boolean dirty) throws ChainException {
        try {
            int index = signInSheet.retrieve(pageno.hashCode());
            if (bufDescr[index].pinCount == 0) {
                throw new PageUnpinnedException(null, "BufMgr: Page not pinned");
            }
            if (dirty) {
                bufDescr[index].dirtyBit = true;
            }
            bufDescr[index].pinCount--;
        } catch (IllegalArgumentException e) {
            throw new HashEntryNotFoundException(null, "bufmgr.HashEntryNotFoundException");
        }
    }
    /**
     * Allocate new pages.
     * Call DB object to allocate a run of new pages and
     * find a frame in the buffer pool for the first page
     * and pin it. (This call allows a client of the Buffer Manager
     * to allocate pages on disk.) If buffer is full, i.e., you
     * can't find a frame for the first page, ask DB to deallocate
     * all these pages, and return null.
     *
     * @param firstpage the address of the first page.
     * @param howmany total number of allocated new pages.
     *
     * @return the first page id of the new pages.__ null, if error.
     */
    public PageId newPage(Page firstpage, int howmany) {
        try {
            PageId pageNum = Minibase.DiskManager.allocate_page(howmany);
            try {
                pinPage(pageNum, firstpage, false);
                return pageNum;
            } catch (ChainException e) {
                Minibase.DiskManager.deallocate_page(pageNum, howmany);
                return null;
            }
        } catch (Exception e) {
          e.printStackTrace();
        }
        return null;
    }
    /**
     * This method should be called to delete a page that is on disk.
     * This routine must call the method in diskmgr package to
     * deallocate the page.
     *
     * @param globalPageId the page number in the data base.
     */
    public void freePage(PageId globalPageId) throws ChainException {
        try {
            evict(signInSheet.retrieve(globalPageId.hashCode()));
            Minibase.DiskManager.deallocate_page(globalPageId);
        } catch (IllegalArgumentException e) {
            //e.printStackTrace();
        }
    }
    /**
     * Used to flush a particular page of the buffer pool to disk.
     * This method calls the write_page method of the diskmgr package.
     *
     * @param pageid the page number in the database.
     */
    public void flushPage(PageId pageid) {
        try {
            int index = signInSheet.retrieve(pageid.hashCode());
            Minibase.DiskManager.write_page(pageid, poolParty[index]);
        } catch (IllegalArgumentException e) {
            //does not need to be flushed
        } catch (ChainException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * Used to flush all dirty pages in the buffer pool to disk
     *
     */
    public void flushAllPages() {
        Arrays.stream(bufDescr)
                .filter(descriptor -> descriptor != null)
                .filter(descriptor1 -> descriptor1.dirtyBit)
                .forEach(descriptor2 -> flushPage(descriptor2.pageId));
    }
    /**
     * Returns the total number of buffer frames.
     */
    public int getNumBuffers() {
        return numbufs;
    }
    /**
     * Returns the total number of unpinned buffer frames.
     */
    public int getNumUnpinned() {
        return (int) Arrays.stream(bufDescr)
                .filter(descriptor -> descriptor == null || descriptor.pinCount == 0)
                .count();
    }

    public static class Descriptor {
        public PageId pageId;
        public int pinCount;
        public boolean dirtyBit;
        public int frequency;
        public int index;
    }
}
