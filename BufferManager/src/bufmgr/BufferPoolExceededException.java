package bufmgr;

import chainexception.ChainException;

/**
 * Created by Aravind on 2/14/16.
 */
public class BufferPoolExceededException extends ChainException{
    public BufferPoolExceededException(Exception e, String name) {
        super(e, name);
    }
}
