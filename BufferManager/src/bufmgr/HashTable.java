package bufmgr;


/**
 * Created by jake on 2/11/16.
 */
public class HashTable {
    //TODO check for equality and duplicate entries
    public static final int HTSIZE = 2039;
    public static final int A = 1;
    public static final int B = 0;
    private Bucket[] directory = new Bucket[HTSIZE];

    public static int hash(int input) {
        return (A * input + B) % HTSIZE;
    }

    public void insert (int pageId, int frameNum) {
        int hash = hash(pageId);

        Bucket cursor = directory[hash];
        Bucket creation = new Bucket(pageId, frameNum, null);

        if (cursor == null) {
            directory[hash] = creation;
        } else {
            while (cursor.next != null) {
                if (cursor.pageId == pageId) {
                    cursor.frameNum = frameNum;
                    return;
                }
                cursor = cursor.next;
            }

            cursor.next = creation;
        }

    }

    public int retrieve(int pageId) throws IllegalArgumentException {
        Bucket cursor = directory[hash(pageId)];

        while (cursor != null && cursor.pageId != pageId)
            cursor = cursor.next;

        if (cursor == null)
            throw new IllegalArgumentException("Index Not Found");

        return cursor.frameNum;
    }

    public void remove(int pageId) throws IllegalArgumentException {
        int hash = hash(pageId);
        Bucket cursor = directory[hash];

        while (cursor != null && cursor.pageId != pageId
                && cursor.next != null && cursor.next.pageId == pageId) {
            cursor = cursor.next;
        }

        if (cursor == null || (cursor.next == null && cursor.pageId != pageId) )
            throw new IllegalArgumentException("Index Not Found");

        if (cursor.next == null) {
            directory[hash] = null;
        } else {
            cursor.next = cursor.next.next;
        }
    }

    public static class Bucket {
        public int pageId;
        public int frameNum;
        Bucket next;

        public Bucket(int pageId, int frameNum, Bucket next) {
            this.pageId = pageId;
            this.frameNum = frameNum;
            this.next = next;
        }
    }
}
