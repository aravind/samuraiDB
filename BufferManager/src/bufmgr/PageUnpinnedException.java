package bufmgr;

import chainexception.ChainException;

/**
 * Created by stuart4 on 2/11/16.
 */
public class PageUnpinnedException extends ChainException{
    public PageUnpinnedException(Exception e, String name) {
        super(e, name);
    }
}
