package heap;

import global.RID;

import java.util.IllegalFormatCodePointException;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

/**
 * Created by jake on 2/17/16.
 */
public class HFIterator implements Iterator<RID> {
    HeapFile.IdentifiedPage identifiedPage = null;
    RID next = null;
    public HFIterator(HeapFile.IdentifiedPage identifiedPage) {
        this.identifiedPage = identifiedPage;

        next = identifiedPage.hfPage.firstRecord();
    }

    @Override
    public boolean hasNext() {
        if (next == null) return false;
        try {
            identifiedPage.hfPage.nextRecord(next);
        } catch (IllegalArgumentException e) {
            return false;
        }
        return true;
    }

    @Override
    public RID next() {
        RID current = next;
        next = identifiedPage.hfPage.nextRecord(next);
        return current;
    }

    @Override
    public void remove() {
        throw new ArithmeticException("You don' fucked up now! ~ Arsenio Hall");
    }

    @Override
    public void forEachRemaining(Consumer<? super RID> action) {
        throw new ArithmeticException("You don' fucked up now! ~ Arsenio Hall");
    }
}
