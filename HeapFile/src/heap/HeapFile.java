package heap;

import chainexception.ChainException;
import global.Minibase;
import global.PageId;
import global.RID;

import java.util.*;

/**
 * Created by jake on 2/14/16.
 */
// stands between the query eval engine and the bufmgr
// http://pages.cs.wisc.edu/~dbbook/openAccess/Minibase/spacemgr/interface/hfpage.h
// http://pages.cs.wisc.edu/~dbbook/openAccess/Minibase/spaceMgr/heap_file.html

    /*
    A heap file is an unordered set of records, stored on a set of pages. This class provides basic support for inserting, selecting, updating, and deleting records. Temporary heap files are used for external sorting and in other relational operators. A sequential scan of a heap file (via the Scan class) is the most basic access method.
     */
public class HeapFile {
    String name;
    int recordCount = 0;
    TreeMap<RID, IdentifiedPage> recordMap = new TreeMap<>((o1, o2) -> Integer.compare(o1.hashCode(), o2.hashCode())); //O(log(n))
    PriorityQueue<IdentifiedPage> spacePriorityQueue = new PriorityQueue<>((IdentifiedPage page1, IdentifiedPage page2)
            -> Integer.compare(page1.hfPage.getFreeSpace(), page2.hfPage.getFreeSpace()));
    List<IdentifiedPage> hfPageList = new LinkedList<>();

    /**
     *
     * @param name If the given name already denotes a file, this opens it; otherwise, this creates a new empty file.
     */
    public HeapFile(String name) {
        PageId result = Minibase.DiskManager.get_file_entry(name);

        if (result == null) { // file not found
            recordCount = 0;

            addAPage(true, name);
        } else { // file found
            HFPage hfPage = new HFPage();
            PageId pageId = Minibase.DiskManager.get_file_entry(name);
            Minibase.BufferManager.pinPage(pageId, hfPage, false); //should this be true? no
            IdentifiedPage identifiedPage = new IdentifiedPage(pageId, hfPage);
            hfPageList.add(identifiedPage);
            if (hfPage.getFreeSpace() >= 4) {
                spacePriorityQueue.add(identifiedPage);
            }
            recordCount = countRecord(); // TODO do not call ourselves
            Minibase.BufferManager.unpinPage(pageId, true);
        }

    }

    private IdentifiedPage addAPage() {
        return addAPage(false, null);
    }

    private IdentifiedPage addAPage(boolean createFile, String filename) {
        HFPage hfPage = new HFPage();
        PageId pageId = Minibase.BufferManager.newPage(hfPage, 1);

        if (createFile)
            Minibase.DiskManager.add_file_entry(filename, pageId);

        Minibase.BufferManager.unpinPage(pageId, createFile);

        IdentifiedPage identifiedPage = new IdentifiedPage(pageId, hfPage);
        hfPageList.add(identifiedPage);
        spacePriorityQueue.add(identifiedPage);

        return identifiedPage;
    }

    /**
     * Inserts a new record into the file and returns its RID
     * Function has a O(log(n))
     * @param record
     * @return
     * @throws ChainException
     */
    public RID insertRecord(byte[] record) throws ChainException {
        if (record.length > HFPage.PAGE_SIZE) {
            throw new SpaceNotAvailableException("heap.SpaceNotAvailableException");
        }

        IdentifiedPage identifiedPage = spacePriorityQueue.poll();
        if (identifiedPage != null && (short) record.length + 4 <= identifiedPage.hfPage.getFreeSpace()) {
            Minibase.BufferManager.pinPage(identifiedPage.pageId, identifiedPage.hfPage, false);
            RID rid = identifiedPage.hfPage.insertRecord(record);
            spacePriorityQueue.add(identifiedPage);
            recordMap.put(rid, identifiedPage);
            Minibase.BufferManager.unpinPage(identifiedPage.pageId, true);
            recordCount++;
            return rid;
        } else {
            identifiedPage = addAPage();
            Minibase.BufferManager.pinPage(identifiedPage.pageId, identifiedPage.hfPage, false);
            RID rid = identifiedPage.hfPage.insertRecord(record);
            if (rid == null) {
                throw new SpaceNotAvailableException("heap.SpaceNotAvailableException");
            }
            recordMap.put(rid, identifiedPage);
            Minibase.BufferManager.unpinPage(identifiedPage.pageId, true);
            recordCount++;
            return rid;
        }
    }

    /**
     * nobody knows
     * maybe it turns an rid into a tuple (where are tuples mentioned?)
     * @param rid
     * @return
     */
    public Tuple getRecord(RID rid) {
        IdentifiedPage identifiedPage = recordMap.get(rid);
        Minibase.BufferManager.pinPage(identifiedPage.pageId, identifiedPage.hfPage, false);
        byte[] record = identifiedPage.hfPage.selectRecord(rid);
        Minibase.BufferManager.unpinPage(identifiedPage.pageId, false);
        return new Tuple(record, 0, record.length);
    }

    /**
     * Function updates the record with the new value given to it
     * Function has a O(log(n))
     * @param rid
     * @param newRecord
     * @return
     * @throws ChainException
     */
    public boolean updateRecord(RID rid, Tuple newRecord) throws ChainException {
        IdentifiedPage identifiedPage = recordMap.get(rid);
        if (identifiedPage != null) {
            Minibase.BufferManager.pinPage(identifiedPage.pageId, identifiedPage.hfPage, false);
            try {
                identifiedPage.hfPage.updateRecord(rid, newRecord);
            } catch (Exception e) {
                throw new InvalidUpdateException();
            }
            checkSpace(identifiedPage);
            Minibase.BufferManager.unpinPage(identifiedPage.pageId, true);
            return true;
        }
        return false;
    }

    private void checkSpace(IdentifiedPage identifiedPage) {
        if (identifiedPage.hfPage.getFreeSpace() >= 4 && !spacePriorityQueue.contains(identifiedPage)) {
			spacePriorityQueue.add(identifiedPage);
		} else if (identifiedPage.hfPage.getFreeSpace() < 4 && spacePriorityQueue.contains(identifiedPage)) {
			spacePriorityQueue.remove(identifiedPage);
		}
    }

    /**
     * Deletes the specified record from the heap file.
     * Function has a O(log(n)
     * @param rid
     * @return
     */
    public boolean deleteRecord(RID rid) {
        IdentifiedPage identifiedPage = recordMap.get(rid);
        if (identifiedPage != null) {
            Minibase.BufferManager.pinPage(identifiedPage.pageId, identifiedPage.hfPage, false);
            identifiedPage.hfPage.deleteRecord(rid);
            checkSpace(identifiedPage);
            Minibase.BufferManager.unpinPage(identifiedPage.pageId, true);
            recordCount--;
            return true;
        }
        return false;
    }

    /**
     * Gets the number of records in the file
     * Function has a O(1)
     * @return
     */
    //get number of records in the file
    public int getRecCnt() {
        return recordCount;
    }

    /**
     * returns the number of records given a filename
     * @return
     */
    private int countRecord() {
        int count = 0;
        for (IdentifiedPage identifiedPage : hfPageList) {
            Minibase.BufferManager.pinPage(identifiedPage.pageId, identifiedPage.hfPage, false);
            RID currentRid = identifiedPage.hfPage.firstRecord();
            if (currentRid != null) {
                count++;
                recordMap.put(currentRid, identifiedPage);
            } else {
                Minibase.BufferManager.unpinPage(identifiedPage.pageId, true);
                continue;
            }
            while (identifiedPage.hfPage.hasNext(currentRid)) {
                currentRid = identifiedPage.hfPage.nextRecord(currentRid);
                recordMap.put(currentRid, identifiedPage);
                count++;
            }
            Minibase.BufferManager.unpinPage(identifiedPage.pageId, true);
        }
        return count;
    }

    /**
     * Initiates a sequential scan of the heap file
     * @return
     */
    public HeapScan openScan() {
        return new HeapScan(this);
    }

    protected Iterator<IdentifiedPage> getIterator() {
        return hfPageList.iterator();
    }
    public static class IdentifiedPage {
        PageId pageId;
        HFPage hfPage;
        public IdentifiedPage(PageId pageId, HFPage hfPage) {
            this.pageId = new PageId();
            this.pageId.copyPageId(pageId);
            this.hfPage = new HFPage();
            this.hfPage.setPage(hfPage);
        }
    }
}
