package heap;

import chainexception.ChainException;
import global.Minibase;
import global.PageId;
import global.RID;

import java.nio.charset.MalformedInputException;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Created by jake on 2/14/16.
 */
public class HeapScan {

    private boolean open = false;
    private Iterator<HeapFile.IdentifiedPage> pageIterator;
    private HFIterator recordIterator;
    private HeapFile.IdentifiedPage cursor;
    private RID currentRid;

    /**
     *Constructs a file scan by pinning the directory header page and initializing iterator fields.
     * @param hf
     */
    protected HeapScan(HeapFile hf) {
        pageIterator = hf.getIterator();
        if (pageIterator.hasNext()) {
            cursor = pageIterator.next();
            recordIterator = new HFIterator(cursor);
            open = true;
            currentRid = cursor.hfPage.firstRecord();
            Minibase.BufferManager.pinPage(cursor.pageId, cursor.hfPage, false);
        } else {
            open = false;
        }
    }

    /**
     * Called by the garbage collector when there are no more references to the object; closes the scan if it's still open.
     * @throws Throwable
     */
    protected void finalize() throws Throwable {
        if (open) close();
    }

    /**
     * Closes the file scan, releasing any pinned pages.
     * @throws ChainException
     */
    public void close() throws ChainException{
        //Minibase.BufferManager.unpinPage(cursor.pageId, false);
        open = false;
        pageIterator = null;
        recordIterator = null;
        cursor = null;
        currentRid = null;
    }

    /**
     *   Returns true if there are more records to scan, false otherwise.
     * @return
     */
    public boolean hasNext() {
        if (open)
            return recordIterator.hasNext() || pageIterator.hasNext();

        return false;
    }

    /**
     *Gets the next record in the file scan.
     * @param rid
     * @return
     */
    public Tuple getNext(RID rid) {

        Tuple tuple = null;
        HeapFile.IdentifiedPage temp = cursor;
        while (cursor != null) {
            if (currentRid != null) {
                rid.pageno = currentRid.pageno;
                rid.slotno = currentRid.slotno;
                byte[] record = cursor.hfPage.selectRecord(currentRid);
                tuple = new Tuple(record, 0, record.length);
                currentRid = cursor.hfPage.nextRecord(currentRid);
                return tuple;
            } else {
                Minibase.BufferManager.unpinPage(cursor.pageId, true);
                temp = cursor;
                try {
                    cursor = pageIterator.next();
                } catch (NoSuchElementException e) {
                    return null;
                }
                if (cursor.pageId.pid != -1) {
                    Minibase.BufferManager.pinPage(cursor.pageId, cursor.hfPage, false);
                    currentRid = cursor.hfPage.firstRecord();
                } else {
                    Minibase.BufferManager.pinPage(cursor.pageId, cursor.hfPage, false);
                }
            }
        }
        Minibase.BufferManager.unpinPage(temp.pageId, true);
        return tuple;
    }
}
