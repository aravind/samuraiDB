package heap;

/**
 * Created by jake on 2/14/16.
 */

import global.Page;

import java.security.InvalidParameterException;
import java.util.stream.IntStream;

/**
 * Each tuple in a relation is a collection of bytes that must fit within a single page. The Tuple class provides a logical view of fields, allowing get and set operations while automatically handling offsets and type conversions.
 */
public class Tuple {
    byte[] data;
    private static final int DEFAULT_BYTE_ARRAY_SIZE = 1024;

    /**
     *
     * @param data
     * @param startIndex
     * @param length
     */
    public Tuple(byte[] data, int startIndex, int length) {
        if (startIndex != 0)
            throw new InvalidParameterException("startIndex is assumed to be 0");

        this.data = new byte[length];

        IntStream.range(startIndex, length)
                .forEach(value -> this.data[value - startIndex] = data[value]);
    }

    /**
     *
     */
    public Tuple() {
        data = new byte[DEFAULT_BYTE_ARRAY_SIZE];
        IntStream.range(0, DEFAULT_BYTE_ARRAY_SIZE)
                .forEach(value -> data[value] = 0);
    }

    /**
     *
     * @return
     */
    public byte[] getTupleByteArray() {
        return data;
    }

    /**
     *
     * @return
     */
    public int getLength() {
        return data.length;
    }
}
