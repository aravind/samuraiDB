package relop;

import global.RID;
import heap.HeapFile;
import heap.HeapScan;

import java.util.stream.IntStream;

/**
 * Wrapper for heap file scan, the most basic access method. This "iterator"
 * version takes schema into consideration and generates real tuples.
 */
public class FileScan extends Iterator {
  private boolean isOpen = false;
  private HeapScan heapScan;
  private HeapFile file;
  private RID rid = new RID();
  /**
   * Constructs a file scan, given the schema and heap file.
   */
  public FileScan(Schema schema, HeapFile file) {
    setSchema(schema);
    this.file = file;

    heapScan = file.openScan();
    isOpen = true;
  }

  /**
   * Gives a one-line explanation of the iterator, repeats the call on any
   * child iterators, and increases the indent depth along the way.
   */
  public void explain(int depth) {
    IntStream
            .range(0, depth)
            .forEach(i -> System.out.printf(" "));
    System.out.println("Hi there, I'm a filescan. Nice to meet you :)");
  }

  /**
   * Restarts the iterator, i.e. as if it were just constructed.
   */
  public void restart() {
    close();
    heapScan = file.openScan();
    isOpen = true;
  }

  /**
   * Returns true if the iterator is open; false otherwise.
   */
  public boolean isOpen() {
    return isOpen;
  }

  /**
   * Closes the iterator, releasing any resources (i.e. pinned pages).
   */
  public void close() {
    isOpen = false;
    heapScan.close();
  }

  /**
   * Returns true if there are more tuples, false otherwise.
   */
  public boolean hasNext() {
    return heapScan.hasNext();
  }

  /**
   * Gets the next tuple in the iteration.
   *
   * @throws IllegalStateException if no more tuples
   */
  public Tuple getNext() {
    return new Tuple(super.schema, heapScan.getNext(rid));
  }

  /**
   * Gets the RID of the last tuple returned.
   */
  public RID getLastRID() {
    return rid;
  }

  public HeapFile getFile() {
    return file;
  }

} // public class FileScan extends Iterator
