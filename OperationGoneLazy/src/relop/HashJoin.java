package relop;

import global.SearchKey;
import heap.HeapFile;
import index.HashIndex;



public class HashJoin extends Iterator {

	private IndexScan outer;
	private IndexScan inner;
	private int outerColumn;
	private int innerColumn;
	private int currentBucket;
	private int currentHash;
	private Tuple nextTuple;
	private Tuple innerTuple;
	private Tuple matches[];
	private HashTableDup hashTableDup;
	int i = 0;

	public HashJoin(Iterator outer, Iterator inner, int outerColumn, int innerColumn) {
		// Assign the inner and outer columns
		this.outerColumn = outerColumn;
		this.innerColumn = innerColumn;

		// Check if outer is an IndexScan. If not, convert it to one
		if (outer instanceof IndexScan) {
			this.outer = (IndexScan) outer;
		}
		else if (outer instanceof FileScan) {
			Schema schema = outer.getSchema();
			HeapFile heapFile = ((FileScan) outer).getFile();
			HashIndex hashIndex = new HashIndex(null);
			while (outer.hasNext()) {
				hashIndex.insertEntry(new SearchKey(outer.getNext().getField(outerColumn)), ((FileScan) outer).getLastRID());
			}
			this.outer = new IndexScan(schema, hashIndex, heapFile);

		}
		else if (outer instanceof KeyScan) {
			Schema schema = outer.getSchema();
			HeapFile heapFile = ((KeyScan) outer).getFile();
			HashIndex hashIndex = ((KeyScan) outer).getIndex();
			this.outer = new IndexScan(schema, hashIndex, heapFile);
		}
		else {
			Schema schema = outer.getSchema();
			HeapFile heapFile = new HeapFile(null);
			while(outer.hasNext()) {
				heapFile.insertRecord(outer.getNext().data);
			}
			HashIndex hashIndex = new HashIndex(null);
			FileScan fileScan = new FileScan(outer.schema, heapFile);
			while (fileScan.hasNext()) {
				hashIndex.insertEntry(new SearchKey(fileScan.getNext().getField(outerColumn)), fileScan.getLastRID());
			}
			this.outer = new IndexScan(schema, hashIndex, heapFile);
		}

		// Check if inner is an IndexScan. If not, convert it to one
		if (inner instanceof IndexScan) {
			this.inner = (IndexScan) inner;
		}
		else if (inner instanceof FileScan) {
			Schema schema = inner.getSchema();
			HeapFile heapFile = ((FileScan) inner).getFile();
			HashIndex hashIndex = new HashIndex(null);
			while (inner.hasNext()) {
				hashIndex.insertEntry(new SearchKey(inner.getNext().getField(innerColumn)), ((FileScan) inner).getLastRID());
			}
			this.inner = new IndexScan(schema, hashIndex, heapFile);
		}
		else if (inner instanceof KeyScan) {
			Schema schema = inner.getSchema();
			HeapFile heapFile = ((KeyScan) inner).getFile();
			HashIndex hashIndex = ((KeyScan) inner).getIndex();
			this.inner = new IndexScan(schema, hashIndex, heapFile);
		}
		else {
			Schema schema = inner.getSchema();
			HeapFile heapFile = new HeapFile(null);
			while(inner.hasNext()) {
				heapFile.insertRecord(inner.getNext().data);
			}
			HashIndex hashIndex = new HashIndex(null);
			FileScan fileScan = new FileScan(inner.schema, heapFile);
			while (fileScan.hasNext()) {
				hashIndex.insertEntry(new SearchKey(fileScan.getNext().getField(innerColumn)), fileScan.getLastRID());
			}
			this.inner = new IndexScan(schema, hashIndex, heapFile);
		}

		// Assign the schema
		this.schema = Schema.join(this.outer.getSchema(), this.inner.getSchema());

		// Initialize the hash table
		hashTableDup = new HashTableDup();
		matches = null;
		currentBucket = 0;
	}

	@Override
	public boolean hasNext() {
		if (matches != null) {
			if (currentBucket == matches.length - 1) {
				currentBucket = 0;
				matches = null;
				return hasNext();
			}
			else {
				while (currentBucket < matches.length) {
					if (innerTuple.getField(innerColumn).equals(matches[currentBucket].getField(outerColumn))) {
						nextTuple = Tuple.join(matches[currentBucket++], innerTuple, schema);
						return true;
					}
					currentBucket++;
				}
				currentBucket = 0;
				matches = null;
				return hasNext();
			}
		}
		else {
			int innerHash = inner.getNextHash();
			if (innerHash != currentHash) {
				buildHashTable(innerHash);
			}
			if (inner.hasNext()) {
				innerTuple = inner.getNext();
				matches = hashTableDup.getAll(new SearchKey(innerTuple.getField(innerColumn).toString()));
				if (matches != null) {
					while (currentBucket < matches.length) {
						if (innerTuple.getField(innerColumn).equals(matches[currentBucket].getField(outerColumn))) {
							nextTuple = Tuple.join(matches[currentBucket], innerTuple, schema);
							return true;
						}
						currentBucket++;
					}
				}
				currentBucket = 0;
				matches = null;
				return hasNext();
			}
			else {
				matches = null;
				return false;
			}
		}
	}

	@Override
	public void explain(int depth) {
		this.indent(depth);
		System.out.println("Hash-Join");
		outer.explain(depth);
		inner.explain(depth);
	}

	@Override
	public void restart() {
		outer.restart();
		inner.restart();
	}

	@Override
	public boolean isOpen() {
		return inner.isOpen() && outer.isOpen();
	}

	@Override
	public void close() {
		inner.close();
		outer.close();
	}

	@Override
	public Tuple getNext() {
		return nextTuple;
	}

	private void buildHashTable(int hash) {
		currentHash = hash;
		outer.restart();
		hashTableDup.clear();
		while(outer.hasNext()) {
			if(outer.getNextHash() == currentHash)
				break;
			outer.getNext();
		}
		while(outer.hasNext()) {
			if (outer.getNextHash() != currentHash)
				break;
			else {
				Tuple outerTuple = outer.getNext();
				hashTableDup.add(new SearchKey(outerTuple.getField(outerColumn).toString()), outerTuple);
			}
		}
	}
}
