package relop;

import global.RID;
import global.SearchKey;
import heap.HeapFile;
import index.BucketScan;
import index.HashIndex;
import index.HashScan;

import java.util.stream.IntStream;

/**
 * Wrapper for bucket scan, an index access method.
 */
public class IndexScan extends Iterator {

    private boolean isOpen = false;
    private HashIndex index;
    private BucketScan bucketScan;
    private HeapFile file;

    /**
     * Constructs an index scan, given the hash index and schema.
     */
    public IndexScan(Schema schema, HashIndex index, HeapFile file) {
        this.schema = schema;
        this.index = index;
        this.file = file;

        this.bucketScan = index.openScan();
        this.isOpen = true;
    }

    /**
     * Gives a one-line explanation of the iterator, repeats the call on any
     * child iterators, and increases the indent depth along the way.
     */
    public void explain(int depth) {
        IntStream
                .range(0, depth)
                .forEach(i -> System.out.printf(" "));
        System.out.println("Index Scan");
    }

    /**
     * Restarts the iterator, i.e. as if it were just constructed.
     */
    public void restart() {
        close();
        this.bucketScan = index.openScan();
        isOpen = true;
    }

    /**
     * Returns true if the iterator is open; false otherwise.
     */
    public boolean isOpen() {
        return isOpen;
    }

    /**
     * Closes the iterator, releasing any resources (i.e. pinned pages).
     */
    public void close() {
        isOpen = false;
        bucketScan.close();
    }

    /**
     * Returns true if there are more tuples, false otherwise.
     */
    public boolean hasNext() {
        return bucketScan.hasNext();
    }

    /**
     * Gets the next tuple in the iteration.
     *
     * @throws IllegalStateException if no more tuples
     */
    public Tuple getNext() {
        RID rid = bucketScan.getNext();
        return new Tuple(schema, file.selectRecord(rid));
    }

    /**
     * Gets the key of the last tuple returned.
     */
    public SearchKey getLastKey() {
        return new SearchKey(bucketScan.getLastKey());
    }

    /**
     * Returns the hash value for the bucket containing the next tuple, or maximum
     * number of buckets if none.
     */
    public int getNextHash() {
        return bucketScan.getNextHash();
    }

} // public class IndexScan extends Iterator
