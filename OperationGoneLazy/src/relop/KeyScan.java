package relop;

import global.RID;
import global.SearchKey;
import heap.HeapFile;
import heap.HeapScan;
import index.HashIndex;
import index.HashScan;

import java.util.stream.IntStream;

/**
 * Wrapper for hash scan, an index access method.
 */
public class KeyScan extends Iterator {

  private boolean isOpen = false;
  private HashIndex index;
  private SearchKey key;
  private HeapFile file;
  private HeapScan heapScan;
  private HashScan indexScan;
  private RID rid = new RID();
  /**
   * Constructs an index scan, given the hash index and schema.
   */
  public KeyScan(Schema schema, HashIndex index, SearchKey key, HeapFile file) {
    this.schema = schema;
    this.index = index;
    this.key = key;
    this.file = file;

    heapScan = file.openScan();
    isOpen = true;
    indexScan = index.openScan(key);
  }

  /**
   * Gives a one-line explanation of the iterator, repeats the call on any
   * child iterators, and increases the indent depth along the way.
   */
  public void explain(int depth) {
    IntStream
            .range(0, depth)
            .forEach(i -> System.out.printf(" "));
    System.out.println("Hi there, I'm a keyscan. Nice to meet you :)");
  }

  /**
   * Restarts the iterator, i.e. as if it were just constructed.
   */
  public void restart() {
    close();
    heapScan = file.openScan();
    indexScan = index.openScan(key);
    isOpen = true;
  }

  /**
   * Returns true if the iterator is open; false otherwise.
   */
  public boolean isOpen() {
    return isOpen;
  }

  /**
   * Closes the iterator, releasing any resources (i.e. pinned pages).
   */
  public void close() {
    isOpen = false;
    heapScan.close();
    indexScan.close();
  }

  /**
   * Returns true if there are more tuples, false otherwise.
   */
  public boolean hasNext() {
    return indexScan.hasNext();
  }

  /**
   * Gets the next tuple in the iteration.
   * 
   * @throws IllegalStateException if no more tuples
   */
  public Tuple getNext() {
    rid = indexScan.getNext();
    // TODO probably doesn't work, should get value at RID
    return new Tuple(schema, heapScan.getNext(rid));
  }

  public HeapFile getFile() {
    return this.file;
  }

  public HashIndex getIndex() {
    return this.index;
  }

} // public class KeyScan extends Iterator
