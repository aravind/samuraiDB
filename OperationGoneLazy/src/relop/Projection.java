package relop;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * The projection operator extracts columns from a relation; unlike in
 * relational algebra, this operator does NOT eliminate duplicate tuples.
 */
public class Projection extends Iterator {

  private Iterator iter;
  private int fields[];

  /**
   * Constructs a projection, given the underlying iterator and field numbers.
   */
  public Projection(Iterator iter, int... fields) {
    this.iter = iter;
    this.fields = fields;
    Schema iteratorSchema = iter.getSchema();

    this.schema = new Schema(fields.length);
    IntStream.range(0, fields.length)
            .forEach(i -> {
              this.schema.initField(i, iteratorSchema.fieldType(fields[i]), iteratorSchema.fieldLength(fields[i]),
                      iteratorSchema.fieldName(fields[i]));
            });
  }

  /**
   * Gives a one-line explaination of the iterator, repeats the call on any
   * child iterators, and increases the indent depth along the way.
   */
  public void explain(int depth) {
    IntStream
            .range(0, depth)
            .forEach(i -> System.out.printf(" "));
    System.out.println("Hi there, I'm a Projection. Nice to meet you :)");
    iter.explain(depth+1);
  }

  /**
   * Restarts the iterator, i.e. as if it were just constructed.
   */
  public void restart() {
    iter.restart();
  }

  /**
   * Returns true if the iterator is open; false otherwise.
   */
  public boolean isOpen() {
    return iter.isOpen();
  }

  /**
   * Closes the iterator, releasing any resources (i.e. pinned pages).
   */
  public void close() {
    iter.close();
  }

  /**
   * Returns true if there are more tuples, false otherwise.
   */
  public boolean hasNext() {
    return iter.hasNext();
  }

  /**
   * Gets the next tuple in the iteration.
   * 
   * @throws IllegalStateException if no more tuples
   */
  public Tuple getNext() {
    Tuple toSlice = iter.getNext();
    List<Object> validList =
            IntStream.of(fields)
            .mapToObj(toSlice::getField)
            .collect(Collectors.toList());
    return new Tuple(this.schema, validList.toArray());
  }

} // public class Projection extends Iterator
