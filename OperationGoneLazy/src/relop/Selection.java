package relop;
import java.util.Arrays;
import java.util.stream.IntStream;

/**
 * The selection operator specifies which tuples to retain under a condition; in
 * Minibase, this condition is simply a set of independent predicates logically
 * connected by OR operators.
 */
public class Selection extends Iterator {

  private Iterator iter;
  private Predicate preds[];
  private Tuple current;
  /**
   * Constructs a selection, given the underlying iterator and predicates.
   */
  public Selection(Iterator iter, Predicate... preds) {
    this.current = null;
    this.iter = iter;
    this.preds = preds;
    this.schema = iter.schema;

    fill();
  }

  private void fill() {
    while (iter.hasNext()) {
      current = iter.getNext();
      if (Arrays.stream(preds)
              .anyMatch(predicate -> predicate.evaluate(current))) {
        // this tuple is valid
        return;
      } else {
        // not a valid tuple
        current = null;
      }
    }
    current = null;
  }

  /**
   * Gives a one-line explanation of the iterator, repeats the call on any
   * child iterators, and increases the indent depth along the way.
   */
  public void explain(int depth) {
    IntStream
            .range(0, depth)
            .forEach(i -> System.out.printf(" "));
    System.out.println("Hi there, I'm a keyscan. Nice to meet you :)");

    iter.explain(depth+1);
  }

  /**
   * Restarts the iterator, i.e. as if it were just constructed.
   */
  public void restart() {
    iter.restart();
  }

  /**
   * Returns true if the iterator is open; false otherwise.
   */
  public boolean isOpen() {
    return iter.isOpen();
  }

  /**
   * Closes the iterator, releasing any resources (i.e. pinned pages).
   */
  public void close() {
    iter.close();
  }

  /**
   * Returns true if there are more tuples, false otherwise.
   */
  public boolean hasNext() {
    return current != null;
  }

  /**
   * Gets the next tuple in the iteration.
   * 
   * @throws IllegalStateException if no more tuples
   */
  public Tuple getNext() {
    Tuple validTuple = current;
    fill();
    return validTuple;
  }

} // public class Selection extends Iterator
