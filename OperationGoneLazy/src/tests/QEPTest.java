package tests;

// MY CODE FOR PART3 IS HERE.

import global.AttrOperator;
import global.AttrType;
import global.Minibase;
import heap.HeapFile;
import relop.*;
import relop.Predicate;
import relop.Schema;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class QEPTest extends TestDriver {

    private HeapFile employees = null;
    private Schema employeeSchema = null;

    private HeapFile departments = null;
    private Schema departmentSchema = null;

    public QEPTest() {

        create_minibase();

        employeeSchema = new Schema(5);
        employeeSchema.initField(0, AttrType.INTEGER, 4, "EmpId");
        employeeSchema.initField(1, AttrType.STRING, 20, "Name");
        employeeSchema.initField(2, AttrType.INTEGER, 4, "Age");
        employeeSchema.initField(3, AttrType.INTEGER, 4, "Salary");
        employeeSchema.initField(4, AttrType.INTEGER, 4, "DeptId");
        employees = new HeapFile(null);

        departmentSchema = new Schema(4);
        departmentSchema.initField(0, AttrType.INTEGER, 4, "DeptId");
        departmentSchema.initField(1, AttrType.STRING, 20, "Name");
        departmentSchema.initField(2, AttrType.INTEGER, 4, "MinSalary");
        departmentSchema.initField(3, AttrType.INTEGER, 4, "MaxSalary");
        departments = new HeapFile(null);

    }

    protected void create_minibase() {
        System.out.println("Creating database...\nReplacer: " + BUF_POLICY);
        new Minibase(DB_PATH, DB_SIZE, BUF_SIZE, BUF_POLICY, false);
    }


    /**
     * Display for each employee his ID, Name and Age
     * SELECT EmpId, Name, Age FROM employees;
     *
     * EmpId, Name, Age, Salary, DeptID
     * 0      1     2    3       4
     * @return
     */
    protected boolean test1() {
        try {
            FileScan fileScan = new FileScan(employeeSchema, employees);
            Projection projection = new Projection(fileScan, 0, 1, 2);
            projection.execute();
            return true;
        } catch (Exception failure) {
            failure.printStackTrace();
            return false;
        }
    }

    /**
     * Display the Name for the departments with MinSalary = MaxSalary
     * SELECT Name FROM departments WHERE MinSalary = MaxSalary
     * @return
     */
    protected boolean test2() {
        try {
            Predicate predicate = new Predicate(AttrOperator.EQ, AttrType.FIELDNO, 2, AttrType.FIELDNO, 3);
            FileScan fileScan = new FileScan(departmentSchema, departments);
            Selection selection = new Selection(fileScan, predicate);
            selection.setSchema(departmentSchema);
            Projection projection = new Projection(selection, 1);
            projection.execute();
            return true;
        } catch (Exception failure) {
            failure.printStackTrace();
            return false;
        }
    }

    /**
     * For each employee, display his Name and the Name of his department as well as the
     maximum salary of his department
     * SELECT e.Name, d.Name, MaxSalary FROM Departments AS d INNER JOIN Employees AS e ON DeptId
     * @return
     */
    protected boolean test3() {
        try {
            FileScan employeeHeapFile = new FileScan(employeeSchema, employees);
            FileScan departmentHeapFile = new FileScan(departmentSchema, departments);
            HashJoin hashJoin = new HashJoin(employeeHeapFile, departmentHeapFile, 4, 0);

            Projection projection = new Projection(hashJoin, 1, 6, 8);
            hashJoin.execute();
            return true;
        } catch (Exception failure) {
            failure.printStackTrace();
            return false;
        }
    }

    /**
     * Display the Name for each employee whose Salary is greater than the maximum salary
     of his department
     * SELECT e.name FROM Employees INNER JOIN Departments ON DeptId WHERE Salary > MaxSalary
     * @return
     */
    protected boolean test4() {
        try {
            FileScan employeeHeapFile = new FileScan(employeeSchema, employees);
            FileScan departmentHeapFile = new FileScan(departmentSchema, departments);
            HashJoin hashJoin = new HashJoin(employeeHeapFile, departmentHeapFile, 4, 0);

            Selection selection = new Selection(hashJoin,
                    new Predicate(AttrOperator.GT, AttrType.FIELDNO, 3, AttrType.FIELDNO, 8));

            Projection projection = new Projection(selection, 1);
            projection.execute();
            return true;
        } catch (Exception failure) {
            failure.printStackTrace();
            return false;
        }
    }

    /**
     * eats employees, poops tuples
     * @param line
     */
    private void consumeEmployee(String line) {
        Tuple tuple = new Tuple(employeeSchema);
        String[] cols = line.split(", ");

        int EmpId = Integer.valueOf(cols[0]);
        String Name = cols[1];
        int Age = Integer.valueOf(cols[2]);
        int Salary = Integer.valueOf(cols[3]);
        int DeptId = Integer.valueOf(cols[4]);

        tuple.setAllFields(EmpId, Name, Age, Salary, DeptId);

        tuple.insertIntoFile(employees);
    }

    /**
     * eats departments, poops tuples
     * @param line
     */
    private void consumeDepartment(String line) {
        Tuple tuple = new Tuple(departmentSchema);
        String[] cols = line.split(", ");

        int DeptId = Integer.valueOf(cols[0]);
        String Name = cols[1];
        int MinSalary = Integer.valueOf(cols[2]);
        int MaxSalary = Integer.valueOf(cols[3]);

        tuple.setAllFields(DeptId, Name, MinSalary, MaxSalary);

        tuple.insertIntoFile(departments);
    }

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        if (args.length != 1) {
            throw new IllegalArgumentException("You didn't pass a directory to QEPTest");
        }

        String dir = args[0];
        if (dir.charAt(dir.length()-1) != '/')
            dir += '/';

        QEPTest qepTest = new QEPTest();

        Stream<String> employeeLines;
        Stream<String> departmentLines;

        try {
            employeeLines = Files.lines(Paths.get(dir + "Employee.txt"));
            departmentLines = Files.lines(Paths.get(dir + "Department.txt"));
        } catch (Exception fatal) {
            throw new IllegalArgumentException("Fatal: Cannot read table data");
        }

        employeeLines.skip(1).forEach(qepTest::consumeEmployee);
        departmentLines.skip(1).forEach(qepTest::consumeDepartment);


        if (qepTest.test1() && qepTest.test2() && qepTest.test3() && qepTest.test4()) {
            System.out.println("All tests passed without exception (check output manually)!");
        } else {
            System.out.println("There was a failure amongst the tests.");
        }
    }
}
