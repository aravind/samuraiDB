package query;

import global.Minibase;
import index.HashIndex;
import parser.AST_CreateIndex;
import relop.Schema;

/**
 * Execution plan for creating indexes.
 *
 *
 */
class CreateIndex implements Plan {
  private AST_CreateIndex tree;

  /**
   * Optimizes the plan, given the parsed query.
   *
   * @throws QueryException if index already exists or table/column invalid
   */
  public CreateIndex(AST_CreateIndex tree) throws QueryException {
    this.tree = tree;
    Schema schema = Minibase.SystemCatalog.getSchema(tree.getIxTable());
    QueryCheck.columnExists(schema, tree.getIxColumn());
    QueryCheck.fileNotExists(tree.getFileName());
    QueryCheck.tableExists(tree.getIxTable());
  } // public CreateIndex(AST_CreateIndex tree) throws QueryException

  /**
   * Executes the plan and prints applicable output.
   */
  public void execute() {
    new HashIndex(tree.getFileName());
    Minibase.SystemCatalog.createIndex(tree.getFileName(), tree.getIxTable(), tree.getIxColumn());
    System.out.println("Index created.");
  } // public void execute()

} // class CreateIndex implements Plan
