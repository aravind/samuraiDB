package query;

import global.Minibase;
import global.RID;
import global.SearchKey;
import heap.HeapFile;
import heap.HeapScan;
import index.HashIndex;
import parser.AST_Delete;
import relop.Predicate;
import relop.Schema;
import relop.Tuple;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Execution plan for deleting tuples.
 */
class Delete implements Plan {
    private Schema schema;
    private AST_Delete tree;
    //Conjunctive normal form
    //              AND OR
    private Predicate[][] predicates;
    private String fileName;
    /**
     * Optimizes the plan, given the parsed query.
     *
     * @throws QueryException if table doesn't exist or predicates are invalid
     */
    public Delete(AST_Delete tree) throws QueryException {
        fileName = tree.getFileName();
        QueryCheck.tableExists(fileName);
        schema = Minibase.SystemCatalog.getSchema(tree.getFileName());
        predicates = tree.getPredicates();
        QueryCheck.predicates(schema, predicates);
    }

    /**
     * Executes the plan and prints applicable output.
     */
    public void execute() {
        HeapFile heapFile = new HeapFile(fileName);
        HeapScan heapScan = heapFile.openScan();
        int countAffected = 0;
        IndexDesc[] indexDescs = Minibase.SystemCatalog.getIndexes(fileName);
        ArrayList<RID> ridArrayList = new ArrayList<>();
        ArrayList<Tuple> tupleArrayList = new ArrayList<>();

        while (heapScan.hasNext()) {
            RID rid = new RID();
            Tuple tuple = new Tuple(schema, heapScan.getNext(rid));

            boolean valid = Arrays.stream(predicates)
                    .allMatch((Predicate[] orPredGroup) -> Arrays.stream(orPredGroup)
                            .anyMatch(predicate -> predicate.evaluate(tuple)));

            if (valid) {
                ridArrayList.add(ridArrayList.size(), rid);
                tupleArrayList.add(tupleArrayList.size(), tuple);
                countAffected++;
            }
        }

        heapScan.close();

        for (int i = 0; i < tupleArrayList.size(); i++) {
            for (IndexDesc indexDesc : indexDescs) {
                SearchKey searchKey = new SearchKey(tupleArrayList.get(i).getField(indexDesc.columnName));
                new HashIndex(indexDesc.indexName).deleteEntry(searchKey, ridArrayList.get(i));
            }

            heapFile.deleteRecord(ridArrayList.get(i));
        }
        Minibase.SystemCatalog.updateRecCount(fileName, Minibase.SystemCatalog.getRecCount(fileName) - countAffected);

        System.out.printf("%d %s affected.\n", countAffected, countAffected == 1 ? "row" : "rows");
    }
}
