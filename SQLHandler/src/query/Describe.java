package query;

import global.Minibase;
import parser.AST_Describe;
import relop.Schema;

/**
 * Execution plan for describing tables.
 */
class Describe implements Plan {

    private Schema schema;

    /**
     * Optimizes the plan, given the parsed query.
     *
     * @throws QueryException if table doesn't exist
     */
    public Describe(AST_Describe tree) throws QueryException {
        String fileName = tree.getFileName();
        QueryCheck.tableExists(fileName);
        this.schema = Minibase.SystemCatalog.getSchema(fileName);
    }

    /**
     * Executes the plan and prints applicable output.
     */
    public void execute() {
        System.out.println(padRight("Name", 32) + padRight("Type", 16));
        for (int i = 0; i < schema.getCount(); i++) {
            System.out.println(padRight(schema.fieldName(i), 32) + padRight(parseType(schema.fieldType(i)), 16));
        }
    }
    private String padRight(String s, int n) {
        return String.format("%1$-" + n + "s", s);
    }

    private String parseType(int type) {
        switch (type) {
            case 11:
                return "INTEGER";
            case 12:
                return "FLOAT";
            case 13:
                return "STRING";
            default:
                return null;
        }
    }
}
