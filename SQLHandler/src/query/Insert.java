package query;

import global.Minibase;
import global.RID;
import global.SearchKey;
import heap.HeapFile;
import index.HashIndex;
import parser.AST_Insert;
import relop.Tuple;

/**
 * Execution plan for inserting tuples.
 */
class Insert implements Plan {

    private AST_Insert tree;
    private String fileName;
    /**
     * Optimizes the plan, given the parsed query.
     *
     * @throws QueryException if table doesn't exists or values are invalid
     */
    public Insert(AST_Insert tree) throws QueryException {
        this.tree = tree;
        this.fileName = tree.getFileName();
        QueryCheck.tableExists(fileName);
        QueryCheck.insertValues(Minibase.SystemCatalog.getSchema(fileName), tree.getValues());
    }

    /**
     * Executes the plan and prints applicable output.
     */
    public void execute() {
        // Insert into table
        HeapFile table = new HeapFile(fileName);
        Tuple tuple = new Tuple(Minibase.SystemCatalog.getSchema(fileName), tree.getValues());
        RID rid = tuple.insertIntoFile(table);

        // Insert into indexes, if available
        IndexDesc[] indexes = Minibase.SystemCatalog.getIndexes(fileName);
        for (IndexDesc index : indexes) {
            SearchKey searchKey = new SearchKey(tuple.getField(index.columnName));
            new HashIndex(index.indexName).insertEntry(searchKey, rid);
        }
        Minibase.SystemCatalog.updateRecCount(fileName, table.getRecCnt());

        // print the output message
        System.out.println("1 row affected.");
    }
}
