package query;

import global.Minibase;
import global.SortKey;
import heap.HeapFile;
import parser.AST_Select;
import relop.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Execution plan for selecting tuples.
 */
class Select implements Plan {
  Predicate[][] predicates;
  SortKey[] sortKeys;
  String[] tables;
  String[] columns;
  /**
   * Optimizes the plan, given the parsed query.
   * 
   * @throws QueryException if validation fails
   */
  public Select(AST_Select tree) throws QueryException {
    columns = tree.getColumns();
    sortKeys = tree.getOrders();
    predicates = tree.getPredicates();
    tables = tree.getTables();

    for (String s : tables) {
      QueryCheck.tableExists(s);
      Schema schema = Minibase.SystemCatalog.getSchema(s);
      for (String column : columns)
        QueryCheck.columnExists(schema, column);
      QueryCheck.predicates(schema, predicates);
    }

    //TODO column existance

  } // public Select(AST_Select tree) throws QueryException

  /**
   * Executes the plan and prints applicable output.
   */
  public void execute() {
    List<Iterator> toFree = new ArrayList<>();
    List<Iterator> fileScanList = Arrays.stream(tables)
            .sorted( (table1, table2) -> {
              //join ordering
              int records1 = Minibase.SystemCatalog.getRecCount(table1);
              int records2 = Minibase.SystemCatalog.getRecCount(table1);
              return Integer.compare(records1, records2);
            })
            .map(table -> {
              Iterator iter =  new FileScan(Minibase.SystemCatalog.getSchema(table), new HeapFile(table));
              toFree.add(iter);
              List<Integer> valids = validPredicates(iter);

              // push ordering
              for (int i : valids) {
                iter = new Selection(iter, predicates[i]);
                toFree.add(iter);
              }

              return iter;
            })
            .collect(Collectors.toList());

    Iterator iterator;
    Predicate[] emptyPreds = {};
    if (fileScanList.size() >= 2) {
      iterator = new SimpleJoin(fileScanList.get(0), fileScanList.get(1), emptyPreds);
      toFree.add(iterator);
      for (int i = 2; i < fileScanList.size(); i++) {
        iterator = new SimpleJoin(iterator, fileScanList.get(i), emptyPreds);
        toFree.add(iterator);
      }
    } else {
      iterator = fileScanList.get(0);
    }

    for (Predicate[] predicatesSeparatedByOr : predicates) {
      iterator = new Selection(iterator, predicatesSeparatedByOr);
      toFree.add(iterator);
    }
    final Schema schema = iterator.getSchema();
    List<Integer> fields = Arrays.stream(columns)
            .map(s -> schema.fieldNumber(s))
            .collect(Collectors.toList());
    Integer[] fieldArray = new Integer[fields.size()];
    if (fields.size() == 0) {
      fields = IntStream.range(0, schema.getCount())
              .boxed()
              .collect(Collectors.toList());
    }
    fieldArray = fields.toArray(fieldArray);
    iterator = new Projection(iterator, fieldArray);

    iterator.execute();


    toFree.forEach(iterator1 -> iterator1.close());

  } // public void execute()

  private List<Integer> validPredicates(Iterator candidate) {
    return IntStream.range(0, predicates.length)
            .filter(i -> {
              return Arrays.stream(predicates[i]).allMatch(predicate -> predicate.validate(candidate.getSchema()));
            }).boxed()
            .collect(Collectors.toList());
  }



} // class Select implements Plan
