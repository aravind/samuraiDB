package query;

import global.Minibase;
import global.RID;
import global.SearchKey;
import heap.HeapFile;
import heap.HeapScan;
import index.HashIndex;
import parser.AST_Update;
import relop.Predicate;
import relop.Schema;
import relop.Tuple;

import java.util.Arrays;

/**
 * Execution plan for updating tuples.
 */
class Update implements Plan {

    private Predicate[][] predicates;
    private String fileName;
    private Schema schema;
    private Tuple replacementTuple;

    /**
     * Optimizes the plan, given the parsed query.
     *
     * @throws QueryException if invalid column names, values, or predicates
     */
    public Update(AST_Update tree) throws QueryException {
        this.fileName = tree.getFileName();
        QueryCheck.tableExists(fileName);
        this.schema = Minibase.SystemCatalog.getSchema(this.fileName);
        this.predicates = tree.getPredicates();
        QueryCheck.predicates(schema, predicates);
        QueryCheck.updateFields(schema, tree.getColumns());
        String columns[] = tree.getColumns();
        int[] fldNos = new int[columns.length];
        for (int i = 0; i < columns.length; i++) {
            fldNos[i] = schema.fieldNumber(columns[i]);
        }
        QueryCheck.updateValues(schema, fldNos, tree.getValues());
        this.replacementTuple = new Tuple(schema, tree.getValues());
    }

    /**
     * Executes the plan and prints applicable output.
     */
    public void execute() {
        HeapFile heapFile = new HeapFile(fileName);
        HeapScan heapScan = heapFile.openScan();
        int countAffected = 0;
        IndexDesc[] indexDescs = Minibase.SystemCatalog.getIndexes(fileName);

        while (heapScan.hasNext()) {
            RID rid = new RID();
            Tuple tuple = new Tuple(schema, heapScan.getNext(rid));

            boolean valid = Arrays.stream(predicates)
                    .allMatch((Predicate[] orPredGroup) -> Arrays.stream(orPredGroup)
                            .anyMatch(predicate -> predicate.evaluate(tuple)));

            if (valid) {
                for (IndexDesc indexDesc : indexDescs) {
                    HashIndex hashIndex = new HashIndex(indexDesc.indexName);
                    SearchKey originalSearchKey = new SearchKey(tuple.getField(indexDesc.columnName));
                    SearchKey replacementSearchKey = new SearchKey(replacementTuple.getField(indexDesc.columnName));
                    try {
                        hashIndex.deleteEntry(originalSearchKey, rid);
                    } catch (IllegalArgumentException e) {
                        continue;
                    }
                    hashIndex.insertEntry(replacementSearchKey, rid);
                }
                heapFile.updateRecord(rid, replacementTuple.getData());
                countAffected++;
            }
        }
        heapScan.close();

        System.out.printf("%d %s affected.\n", countAffected, countAffected == 1 ? "row" : "rows");
    }
}
